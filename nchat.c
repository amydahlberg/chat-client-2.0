#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/time.h>
#include <ncurses.h>
#include <sys/select.h>

//written by Amy Dahlberg
//http://www.tutorialspoint.com/unix_system_calls/_newselect.htm

main()
{
  char *username = (char *) malloc (10 * sizeof (char));
  int i = 1;
  char chat[80]; //storing the input
  char user[80];
  char *quit = "q\n";
  char buffer[80] = {0}; //buffer to receive messages
  int status;
  fd_set rfds;
  struct timeval tv;
  int retval;

  /* Wait up to five seconds. */
  tv.tv_sec = 5;
  tv.tv_usec = 0;

//keeping all the server and socket parts together
  int s1 = socket(AF_INET,SOCK_STREAM,0); //making a socket
  struct sockaddr_in server; //server settings
  int servlen;
  server.sin_family = AF_INET;
  server.sin_port = htons(49153);
  inet_pton(AF_INET,"10.115.20.250",&server.sin_addr); //IP address, connecting
  status = connect(s1,(const struct sockaddr*)&server,sizeof(server)); //connect to server
  if (status != 0){
    printf("Could not connect to server");
    exit(1);
  }

//function to make a window, but it was not taking input so i do not use it
WINDOW *window(int y0, int x0, int height, char text[100]){
  WINDOW *chatbox;

  initscr();

  chatbox = newwin(10,80,20,0);
  refresh();
  box(chatbox, 0 , 0);

  mvwprintw(chatbox,1,1, text);

  wrefresh(chatbox);
  memset(text,0,100);
}
//getting ready for the select function later
  FD_ZERO(&rfds);
  FD_SET(0, &rfds);
  FD_SET(s1+1, &rfds);
  printf("username:  ");
  fgets(username,80,stdin);
  strcpy(user,username); //formatting the username
  user[strlen(user)-1] = 0; //taking off newline character
  strcat(user,":  ");
  send(s1,username,strlen(username),0); //username to server
  //draw a window and begin storing text in it
  //can get a window drawn in ncurses but no input was being taken in
  /*
  initscr();
  WINDOW *box1 = newwin(10,100,20,0);
  refresh();
  box(box1,0,0);
  mvwprintw(box1,i,i,"%s");
  wrefresh(box1);
  delwin(box1);
  */
  i = i + 1;
  printf("%s",user);
  fgets(chat,80,stdin); //storing input in chat
  send(s1,chat,strlen(chat),0); //need this i guess
  /*
  mvwprintw(box1,i,i,"%s");
  WINDOW *box2 = newwin(30,100,0,0);
  refresh();
  box(box2,0,0);
  mvwprintw(box2,i,i,"%s");
  wrefresh(box2);
  delwin(box2);
  */
  recv(s1,buffer,80,0);
  printf("\n%s\n",buffer);
  memset(buffer,0,sizeof(buffer));
  retval = select(fileno(stdin)+1, &rfds, NULL, NULL, &tv);

  //receive information from server while not sending input
    while (strcmp(chat,quit) != 0){ //go through loop if chat isn't q
      memset(chat,0,strlen(chat)); //clear where the input is stored
      printf("%s",user);
      fgets(chat,80,stdin);
      retval = select(fileno(stdin)+1, &rfds, NULL, NULL, &tv);
      if (retval == -1){ //leave this alone
          perror("select()");
        }
      else if (FD_ISSET(0, &rfds)){
          send(s1,chat,strlen(chat),0); //send the input to server
          //mvwprintw(box1,i,i,"%s");
          }
      else{
        recv(s1,buffer,80,0);
        printf("\n%s\n",buffer);
        //mvwprintw(box2,i,i,"%s");
        memset(buffer,0,sizeof(buffer));
        }
      i = i + 1;
      FD_ZERO(&rfds);
      FD_SET(0, &rfds);
      FD_SET(s1+1, &rfds);
      /* Make sure that it keeps resetting to 5 seconds */
      tv.tv_sec = 5;
      tv.tv_usec = 0;

    }
  free(username);
  /*
  delwin(box1);
  delwin(box2);
  endwin();
  */
  close(s1); //close socket
}
